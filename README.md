# Jobs dataset

Parsed Australian jobs dataset containing ~5.8 million de-duplicated online job adverts between March 2021 - October 2022.

As the CSV file is large (~1GB), the file has been compressed as a ZIP file.

## Columns
| Column name      | Description |
| ----------- | ----------- |
| `date_posted`    | Date when the first job advertisement was posted. |
| `location_area`    | Specified area for the job ad (e.g. a local government area, state suburb) |
| `location_state`   | Specified Australian state for the job ad |
| `experience_min` | The minimum amount of years required to apply for a job vacancy. This does not count educational experience (e.g. years spent in university or adult course), but rather counts post-qualification experience or experience with a hard skill that is not education related. |
| `age_min`, `age_max` | Minimum and maximum age required for candidate to apply for the job. |
| `edu_min` | Minimum attained education level required to apply. It is either one of these values: *(null), highschool, trade, uni*. |
| `work_type` | Type of employment. Values: *(null), casual, full, fixed, part, other*. |
| `pay_min`, `pay_max`, `pay_rate` | Stated minimum and maximum income and the rate of pay. Rate of pay can be: *(null), hourly, daily, weekly, fortnightly, monthly, year*. |
| `pay_min_normalised`,	`pay_max_normalised` | Normalised hourly pay rate based on the provided `pay_rate`. |
| `anzsco_code` | Full ANZSCO code detected for a job. |
| `anzsco_major_group`,	`anzsco_submajor_group`,	`anzsco_minor_group` | ANZSCO code split into three levels: major, submajor and minor based off from `anzsco_code`. Any of these can be null if `anzsco_code` is null, or if `anzsco_code` does not specify at that level (i.e. due to pipeline being unable to infer the occupational code for the advert).  |
| `anzsic_division`,	`anzsic_subdivision` | Estimated/predicted ANZSIC code of the advertisted company who had listed the job vacancy. |
| `company_via_recruit_agency` | If the ad was posted by a third-party recruitment agency. It is either *True* or *False*. |
| `anzsic_division_adjusted`,	`anzsic_subdivision_adjusted` | Predicted ANZSIC code of the advertisted company, or the company that is using a recruting agency to list the job vacancy. It will equal to the corresponding 'unadjusted' code if `company_via_recruit_agency = False`, and may differ if `company_via_recruit_agency = True`. |
| `discrim_disability` | Number of phrases in the job description that are either explicitly ableist or implicitly ageist and/or ableist (i.e. it is not necessarily offensive but it may discourage such affected candidates from applying). |
| `discrim_ageist` | Number of found phrases in the job advert suggesting possibly ageist language. |
| `discrim_agentic` | Number of found phrases in the job advert suggesting agentic (masculine) traits. |
| `discrim_communal` | Number of phrases suggesting communal (feminine) traits. |
| `discrim_gendered_nouns` | Number of phrases where the noun is gendered (e.g. fireman). |
| `softskill_*` | Binary variables representing the presence or absence of a soft skill in a job advert (0 if absent, 1 if advert indicates need of the soft skill). There are 45 soft skill columns in total. |
